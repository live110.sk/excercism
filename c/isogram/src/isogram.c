#include <memory.h>
#include <ctype.h>
#include "isogram.h"

bool is_isogram(const char phrase[]) {
    if (!(phrase)) {
    return false;
}
    for (long unsigned int i = 0; i < strlen(phrase); i++)
        if (phrase[i] != '-' && phrase[i] != ' ')
            for (int y = i - 1; y >= 0; y--) {
                if (toupper(phrase[i]) == toupper(phrase[y]))
                    return false;
            }
    return true;
}
