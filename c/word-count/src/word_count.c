#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <stdlib.h>
#include "string.h"
#include "word_count.h"

int count_one_word(char *current_word, word_count_word_t *words);

int count_one_word(char *current_word, word_count_word_t *words) {
    if(strlen(current_word)>0)
    {
        if(current_word[strlen(current_word)-1]=='\'')
            current_word[strlen(current_word)-1]='\0';
        if(current_word[0]=='\''){
            char * current_word_iter = current_word+1;
            strcpy(current_word,current_word_iter);
        }
    }
    else return 0;

    for (word_count_word_t *words_iter = words; ;words_iter++){
        if(strlen(words_iter[0].text)==0){
            words_iter[0].count=1;
            strcpy(words_iter[0].text,current_word);
            printf("(new)");
            return 1;
        }else if(strcmp(words_iter[0].text,current_word)==0){
            words_iter[0].count++;
            printf("(+%i)",words_iter[0].count);
            return 0;
        }
    }
}

int word_count(const char *input_text, word_count_word_t *words) {
    memset(words, 0, MAX_WORDS * sizeof(word_count_word_t));
    int total_count = 0;
    char *normalized_text = malloc(strlen(input_text)+1);
    strcpy(normalized_text,input_text);
    char *token_separator = malloc(strlen(input_text)+1);
//    memset(token_separator, 0,strlen(input_text)+1);
    for (char *text_iterator = normalized_text,*token_separator_iter = token_separator; strlen(text_iterator) > 0; text_iterator++) {
        if(!isalpha(text_iterator[0])&&!isdigit(text_iterator[0])&&text_iterator[0]!='\''&&strchr(token_separator,text_iterator[0])==NULL) {
            token_separator_iter[0]=text_iterator[0];
            token_separator_iter++;
        }
        else
            text_iterator[0]=tolower(text_iterator[0]);
    }
    printf("\n\n--------------------------------------------------\n"
           "string:%s\n"
           "normalized:%s\n"
           " token_separator(%lu):%s \n"
           ,input_text,normalized_text,strlen(token_separator),token_separator);
    char *current_word=strtok(normalized_text,token_separator);
    while(current_word!=NULL){
        printf("-%s",current_word);
        if(strlen(current_word)>MAX_WORD_LENGTH) {
            free(normalized_text);
            return EXCESSIVE_LENGTH_WORD;

        }
        total_count+=count_one_word(current_word,words);
        current_word=strtok(NULL,token_separator);
    }





    printf(" %i words ----------------------------------------------------\n\n",total_count);
    free(normalized_text);
    return total_count;
}


