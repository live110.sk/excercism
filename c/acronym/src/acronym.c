#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include "acronym.h"
#include "string.h"

char *abbreviate(const char *phrase) {
    if (!phrase || strlen(phrase) == 0)
        return NULL;
    char *buffer = malloc(strlen(phrase) + 1);
    char *bufferIter = buffer;
    for (unsigned long i = 0, not_need_space = 1; i < strlen(phrase); i++) {
        if (phrase[i] == ' ' || phrase[i] == '-')
            not_need_space = 1;
        else if (not_need_space) {
            *(bufferIter++) = (char) toupper(phrase[i]);
            not_need_space = 0;
        }
    }
    *(bufferIter)='\0';
    return buffer;
}
